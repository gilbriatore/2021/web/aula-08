/************************************************************************************
    AULA 2: Javascript funcional e orientado a objetos
    1. Funções: literais, anônimas, arrows e callback;
    2. Orientação a objetos: objetos literais, função construtora, classes e JSON;
    3. Arrays: funções forEach, map, filter e reduce.    
 ***********************************************************************************/

// VARIÁVEIS + OPERADORES + ESTRUTURAS + FUNÇÕES + OBJETOS

//1. Funções: literais, anônimas, arrows e callback;
// Função literal
function somarLiteral(a, b){
  return a + b;
}

//console.log(somarLiteral(10, 5));

// Função seta (Arrow function)
const somar = (a, b) => a + b;
const subtrair = (a, b) => a - b;
const multiplicar = (a, b) => a * b;
const dividir = (a, b) => a / b;
const modulo = (a, b) => a % b;

// console.log(somar(10, 5));
// console.log(subtrair(10, 5));
// console.log(multiplicar(10, 5));
// console.log(dividir(10, 5));
// console.log(modulo(13, 5));

// Função anônima
const fnSomar = function(a, b){
  return a + b;
}
// console.log(fnSomar(10, 5));

const retorno = function(a, b){
  return a + b;
}(10, 5);
// console.log(retorno);

// (function(a, b){ console.log(a + b); }(10, 5));


// Função de callback (chamada posterior)
function fnCallBack(processo, tamanho){
  for (let i = 0; i < tamanho; i++) {
    console.log(processo + ": " + i);
  }
}

function processo1(){ 
  const fn = fnCallBack("Processo 1", 100000);
  setTimeout(fn, 100);
}

function processo2(){  
  const fn = fnCallBack("Processo 2", 10000);
  setTimeout(fn, 50);
}

function processo3(){ 
  const fn = fnCallBack("Processo 3", 1000);
  setTimeout(fn, 75);
}

// Qual a ordem de execução?
// 2 3 1
// Qual a ordem de término dos processos?
// 2 3 1
// processo1(); //100ms
// processo2(); // 50ms 
// processo3(); // 75ms

//2. Orientação a objetos: objetos literais, função construtora, classes e JSON;

// Objeto literal
const objetoLiteral = {};
objetoLiteral.nome = "João";
objetoLiteral.idade = 30;
objetoLiteral.getIdade = function(){
  return this.idade;
}

// console.log(objetoLiteral.getIdade());

// Função construtora
const Pessoa = function(){
  nome: String;
  idade: Number
}

const p = new Pessoa();
p.nome = "Maria";
p.idade = 25;

// console.log(p);

// Forma classes
class Cliente{
  constructor(nome, idade){
    this.nome = nome;
    this.idade = idade;
  }
}

const c = new Cliente("Pedro", 23);
// console.log(c);

// Objetos no formato JSON > objetos Javascript

const array = require('./produtos.json');

// for (let i = 0; i < array.length; i++) {
//   const produto = array[i];
//   console.log(produto.id + " " + produto.nome);
// }

// for(i in array){
//   const produto = array[i];
//   console.log(produto.id + " " + produto.nome);
// }

// for (produto of array) {
//   console.log(produto.id + " " + produto.nome);
// }


// 3. Arrays: funções forEach, map, filter e reduce.  

//array.forEach(produto => console.log(produto.id + " " + produto.nome));

const nomes = array.map(produto => produto.id + " " + produto.nome);
// nomes.forEach(str => console.log(str));

const produtos = array.filter(produto => !produto.ehVegetariano);
// console.log(produtos);

const precos = array.map(produto => produto.preco);
const fnTotalizar = (totalizador, preco) => totalizador + preco;
const total = precos.reduce(fnTotalizar);
console.log(total.toFixed(2));